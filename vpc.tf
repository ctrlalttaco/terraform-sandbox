locals {
  availability_zones = ["us-west-2a", "us-west-2b", "us-west-2c"]

  primary_vpc = {
    vpc_cidr             = "10.0.0.0/16",
    private_subnet_cidrs = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"],
    public_subnet_cidrs  = ["10.101.0.0/24", "10.102.0.0/24", "10.103.0.0/24"]
  }

  secondary_vpc = {
    vpc_cidr             = "10.1.0.0/16",
    vpc_secondary_cidrs  = ["100.64.0.0/16"],
    private_subnet_cidrs = ["10.1.1.0/24", "10.1.2.0/24", "10.1.3.0/24"],
    intra_subnet_cidrs   = ["100.64.1.0/24", "100.64.2.0/24", "100.64.3.0/24"]
  }
}

module "vpc_a" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.13.0"

  name = "vpc-a"

  azs             = local.availability_zones
  cidr            = local.primary_vpc.vpc_cidr
  private_subnets = local.primary_vpc.private_subnet_cidrs
  public_subnets  = local.primary_vpc.public_subnet_cidrs

  enable_nat_gateway = true
  single_nat_gateway = false

  enable_dns_hostnames = true
  enable_dns_support   = true
}

module "vpc_b" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.13.0"

  name = "vpc-b"

  azs                   = local.availability_zones
  cidr                  = local.secondary_vpc.vpc_cidr
  secondary_cidr_blocks = local.secondary_vpc.vpc_secondary_cidrs
  private_subnets       = local.secondary_vpc.private_subnet_cidrs
  intra_subnets         = local.secondary_vpc.intra_subnet_cidrs

  enable_nat_gateway = false
  single_nat_gateway = false

  enable_dns_hostnames = true
  enable_dns_support   = true
}

resource "aws_nat_gateway" "vpc_b" {
  connectivity_type = "private"
  subnet_id         = module.vpc_b.private_subnets[0]
}

module "tgw" {
  source = "./modules/terraform-aws-transit-gateway-2.5.1"

  name            = "tgw"
  description     = "Transit Gateway"
  amazon_side_asn = 64532
  share_tgw       = false

  enable_default_route_table_association = false
  enable_default_route_table_propagation = false
  enable_propogation                     = false

  vpc_attachments = {
    vpc1 = {
      vpc_id     = module.vpc_a.vpc_id
      subnet_ids = module.vpc_a.private_subnets

      transit_gateway_default_route_table_association = false
      transit_gateway_default_route_table_propagation = false

      dns_support  = true
      ipv6_support = false

      tgw_routes = [
        {
          destination_cidr_block = local.primary_vpc.vpc_cidr
        },
        {
          destination_cidr_block = "0.0.0.0/0"
        }
      ]
    },
    vpc2 = {
      vpc_id                                          = module.vpc_b.vpc_id
      subnet_ids                                      = module.vpc_b.private_subnets
      transit_gateway_default_route_table_association = false
      transit_gateway_default_route_table_propagation = false

      dns_support  = true
      ipv6_support = false

      tgw_routes = [
        {
          destination_cidr_block = local.secondary_vpc.vpc_cidr
        },
        {
          destination_cidr_block = local.secondary_vpc.vpc_secondary_cidrs[0]
          blackhole              = true
        }
      ]
    }
  }
}

# resource "aws_route" "vpc_a_public_route" {
#   route_table_id         = module.vpc_a.public_route_table_ids[0]
#   destination_cidr_block = module.vpc_b.vpc_cidr_block
#   transit_gateway_id     = module.tgw.ec2_transit_gateway_id
# }

# resource "aws_route" "vpc_a_private_route" {
#   route_table_id         = module.vpc_a.private_route_table_ids[0]
#   destination_cidr_block = module.vpc_b.vpc_cidr_block
#   transit_gateway_id     = module.tgw.ec2_transit_gateway_id
# }

# resource "aws_route" "vpc_b_private_route" {
#   route_table_id         = module.vpc_b.private_route_table_ids[0]
#   destination_cidr_block = module.vpc_a.vpc_cidr_block
#   transit_gateway_id     = module.tgw.ec2_transit_gateway_id
# }

# resource "aws_route" "vpc_b_private_default_route" {
#   route_table_id         = module.vpc_b.private_route_table_ids[0]
#   destination_cidr_block = "0.0.0.0/0"
#   transit_gateway_id     = module.tgw.ec2_transit_gateway_id
# }

# resource "aws_route" "vpc_b_intra_default_route" {
#   route_table_id         = module.vpc_b.intra_route_table_ids[0]
#   destination_cidr_block = "0.0.0.0/0"
#   nat_gateway_id         = aws_nat_gateway.vpc_b.id
# }
