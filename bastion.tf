data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "tls_private_key" "this" {
  algorithm = "RSA"
}

resource "local_file" "tls" {
  content         = tls_private_key.this.private_key_pem
  filename        = "${path.module}/ec2.pem"
  file_permission = "0600"
}

module "key_pair" {
  source  = "terraform-aws-modules/key-pair/aws"
  version = "~> 1.0"

  key_name   = "bastion"
  public_key = tls_private_key.this.public_key_openssh
}

### BASTION for VPC-A (Public)

module "bastion_a_profile" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  version = "~> 4"

  create_role             = true
  create_instance_profile = true

  role_name             = "bastion-a"
  role_path             = "/profile/"
  trusted_role_services = ["ec2.amazonaws.com"]

  custom_role_policy_arns           = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
  number_of_custom_role_policy_arns = 1
}

module "bastion_a_ssh" {
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  version = "~> 4"

  name        = "bastion-a-ssh"
  description = "Bastion SSH Access"
  vpc_id      = module.vpc_a.vpc_id

  auto_egress_rules   = ["all-all"]
  ingress_cidr_blocks = ["98.38.107.80/32", "10.0.0.0/8"]
}

module "bastion_a" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name                        = "bastion-a"
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  key_name                    = module.key_pair.key_pair_key_name
  iam_instance_profile        = module.bastion_a_profile.iam_instance_profile_name
  monitoring                  = true
  vpc_security_group_ids      = [module.bastion_a_ssh.security_group_id]
  subnet_id                   = module.vpc_a.public_subnets[0]
  associate_public_ip_address = true
}

output "bastion_a_public_ip" {
  value = module.bastion_a.public_ip
}

### BASTION for VPC-B (Private)

module "bastion_b_profile" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  version = "~> 4"

  create_role             = true
  create_instance_profile = true

  role_name             = "bastion-b"
  role_path             = "/profile/"
  trusted_role_services = ["ec2.amazonaws.com"]

  custom_role_policy_arns           = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
  number_of_custom_role_policy_arns = 1
}

module "bastion_b_ssh" {
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  version = "~> 4"

  name        = "bastion-b-ssh"
  description = "Bastion SSH Access"
  vpc_id      = module.vpc_b.vpc_id

  auto_egress_rules   = ["all-all"]
  ingress_cidr_blocks = ["10.0.0.0/8"]
}

module "bastion_b" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name                        = "bastion-b"
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  key_name                    = module.key_pair.key_pair_key_name
  iam_instance_profile        = module.bastion_b_profile.iam_instance_profile_name
  monitoring                  = true
  vpc_security_group_ids      = [module.bastion_b_ssh.security_group_id]
  subnet_id                   = module.vpc_b.private_subnets[0]
  associate_public_ip_address = false
}

output "bastion_b_private_ip" {
  value = module.bastion_b.private_ip
}

### BASTION for VPC-B (Intranet)

module "bastion_c_profile" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  version = "~> 4"

  create_role             = true
  create_instance_profile = true

  role_name             = "bastion-c"
  role_path             = "/profile/"
  trusted_role_services = ["ec2.amazonaws.com"]

  custom_role_policy_arns           = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
  number_of_custom_role_policy_arns = 1
}

module "bastion_c_ssh" {
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  version = "~> 4"

  name        = "bastion-c-ssh"
  description = "Bastion SSH Access"
  vpc_id      = module.vpc_b.vpc_id

  auto_egress_rules   = ["all-all"]
  ingress_cidr_blocks = ["10.0.0.0/8"]
}

module "bastion_c" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name                        = "bastion-c"
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  key_name                    = module.key_pair.key_pair_key_name
  iam_instance_profile        = module.bastion_c_profile.iam_instance_profile_name
  monitoring                  = true
  vpc_security_group_ids      = [module.bastion_c_ssh.security_group_id]
  subnet_id                   = module.vpc_b.intra_subnets[0]
  associate_public_ip_address = false
}

output "bastion_c_private_ip" {
  value = module.bastion_c.private_ip
}
